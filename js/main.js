$(function(){

	$('.auto-slider').slick({  
		infiniti: true,
		slidesToShow: 3,
		slidesToScroll: 1,
		arrows: false,
		dots:true,
		responsive: [
			{
				breakpoint: 916,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 1,
					infinite: true,
					dots: true
				},
			},
			{
				breakpoint: 741,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
					infinite: true,
					dots: true
				},
				
			},
		]
	})
});